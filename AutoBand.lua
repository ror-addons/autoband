-- AutoBand.lua

AutoBand = {}


function AutoBand.init()
	AutoBand.cmd_queue = {}				-- global command queue
	AutoBand.cmd_current = nil			-- current queue command to be executed
	AutoBand.debugon = false			-- debug flag
	AutoBand.gizilon = false			-- gizil debug flag
	AutoBand.offliners_countdown = {}	-- offline players and their remaining time
	AutoBand.has_shortslash = false		-- true if we can register /ab
	AutoBand.elapsed = 0				-- elapsed update time

--[[	AutoBand.buffTargets = {}
	for i = GameData.BuffTargetType.GROUP_MEMBER_START, GameData.BuffTargetType.SELF do
		table.insert(AutoBand.buffTargets, #AutoBand.buffTargets + 1, i) 
	end
	table.insert(AutoBand.buffTargets, #AutoBand.buffTargets + 1, GameData.BuffTargetType.TARGET_FRIENDLY)
]]--
	-- persistent data
	AutoBand.saved = AutoBand.saved or {}

	if (AutoBand.saved.autokick_enabled == nil) then
		AutoBand.saved.autokick_enabled = AB_const.AUTOKICK	-- auto kick flag
	end
	AutoBand.saved.autokick_period = AutoBand.saved.autokick_period or AB_const.KICK_PERIOD
	AutoBand.saved.min_rank = AutoBand.saved.min_rank or AB_const.MINIMUM_RANK
	AutoBand.saved.default_template = AutoBand.saved.default_template or AB_const.EMPTY_TEMPLATE	-- default template to be applied
	AutoBand.saved.templates = AutoBand.saved.templates or {}

	--[[ DEBUG PURPOSES ]]--
	AutoBand.saved.dumps = AutoBand.saved.dumps or {}
	AutoBand.saved.dumped_wb = AutoBand.saved.dumped_wb or nil
	AutoBand.saved.default_dump = AutoBand.saved.default_dump or nil
	--[[ DEBUG PURPOSES ]]--

	AutoBand.saved.org_algo_mode = AutoBand.saved.org_algo_mode or AB_const.DEFAULT_ORG_ALGO
	AutoBand.saved.org_algo_role = AutoBand.saved.org_algo_role or AB_const.DEFAULT_ORG_ROLE

	if (AutoBand.saved.displayicon == nil) then
		AutoBand.saved.displayicon = true
	end

	if (AutoBand.saved.kick_func_enabled == nil) then
		AutoBand.saved.kick_func_enabled = {}
		for k, v in pairs(AB_const.KICK_FUNC_DEFAULT) do
			AutoBand.saved.kick_func_enabled[k] = v
		end
	end

	-- register LibSlash command "/autoband" and tries "/ab"
	LibSlash.RegisterSlashCmd("autoband", function (msg) AutoBand.parse_cmd(msg) end)
	if (not LibSlash.IsSlashCmdRegistered("ab")) then
		LibSlash.RegisterSlashCmd("ab", function (msg) AutoBand.parse_cmd(msg) end)
		AutoBand.has_shortslash = true
	end

	-- load command list
	AutoBand.cmd = {}
	AutoBand.cmd[""] = { ["f"] = AutoBand.cmd_usage, ["help"] = "" , ["print_id"] = 10}
	AutoBand.cmd["help"] = { ["f"] = AutoBand.cmd_usage, ["help"] = "show command list" , ["print_id"] = 11}
	AutoBand.cmd["info"] = { ["f"] = AutoBand.cmd_show, ["help"] = "show current settings" , ["print_id"] = 12}
	AutoBand.cmd["show"] = { ["f"] = AutoBand.cmd_toggle_gui, ["help"] = "open/close GUI" , ["print_id"] = 12}
	
	AutoBand.cmd["org"] = { ["f"] = AutoBand.cmd_organize, ["help"] = "organize the warband" , ["print_id"] = 20}
	

	AutoBand.cmd["offliners"] = { ["f"] = AutoBand.cmd_list_offline, ["help"] = "list all offline players" , ["print_id"] = 30}
	AutoBand.cmd["kickoff"] = { ["f"] = AutoBand.cmd_kick_offline, ["help"] = "kick all offline players" , ["print_id"] = 31}
	AutoBand.cmd["autokick"] = { ["f"] = AutoBand.cmd_flag_autokick, ["help"] = "toggle auto kick offline players" , ["print_id"] = 32}
	AutoBand.cmd["kicktimeout"] = { ["f"] = AutoBand.cmd_autokick_timeout, ["help"] = "<number> sets autokick timeout in minutes" , ["print_id"] = 33}

	AutoBand.cmd["zone"] = { ["f"] = AutoBand.cmd_list_zone, ["help"] = "list all players' not in leader's / assistants' zone" , ["print_id"] = 40}
	AutoBand.cmd["zonekick"] = { ["f"] = AutoBand.cmd_kick_notinzone, ["help"] = "kick all players not in leader's / assistants' zone" , ["print_id"] = 41}	
	AutoBand.cmd["rank"] = { ["f"] = AutoBand.cmd_list_rank, ["help"] = "list all players' not in leader's / assistants' zone" , ["print_id"] = 50}
	AutoBand.cmd["minrank"] = { ["f"] = AutoBand.cmd_rank, ["help"] = "<number> sets the minimum rank requirement" , ["print_id"] = 51}
	AutoBand.cmd["rankkick"] = { ["f"] = AutoBand.cmd_kick_rank, ["help"] = "kick all players less than required rank" , ["print_id"] = 52}


	AutoBand.cmd["list"] = { ["f"] = AutoBand.cmd_list_template, ["help"] = "list all the templates" , ["print_id"] = 60}
	AutoBand.cmd["save"] = { ["f"] = AutoBand.cmd_save_template, ["help"] = "<name> save the current warband as a template" , ["print_id"] = 61}
	AutoBand.cmd["apply"] = { ["f"] = AutoBand.cmd_apply_template, ["help"] = "<name> apply the template" , ["print_id"] = 62}
	AutoBand.cmd["default"] = { ["f"] = AutoBand.cmd_default_template, ["help"] = "sets to <name> or clear the default template" , ["print_id"] = 63}
	AutoBand.cmd["mode"] = { ["f"] = AutoBand.cmd_mode, ["help"] = "<number> sets the distribution algorithm mode" , ["print_id"] = 64}
	
	AutoBand.cmd["icon"] = { ["f"] = AutoBand.cmd_toggle_icon, ["help"] = "show/hide the minimap icon" , ["print_id"] = 70}

	AutoBand.cmd["dump"] = { ["f"] = AutoBand.cmd_dump, ["help"] = "" , ["print_id"] = 80} -- dump groups for debug purpose
	AutoBand.cmd["cleardump"] = { ["f"] = AutoBand.cmd_cleardumps, ["help"] = "" , ["print_id"] = 81} -- clears last wb dump
	AutoBand.cmd["listdump"] = { ["f"] = AutoBand.cmd_list_dump, ["help"] = "" , ["print_id"] = 83}
	AutoBand.cmd["loaddump"] = { ["f"] = AutoBand.cmd_load_dump, ["help"] = "" , ["print_id"] = 84}
	AutoBand.cmd["tempdump"] = { ["f"] = AutoBand.cmd_dump_from_template, ["help"] = "" , ["print_id"] = 85}
	AutoBand.cmd["debug"] = { ["f"] = AutoBand.cmd_debug, ["help"] = "" , ["print_id"] = 100} -- toggle debug mode

	AutoBand.cmd_ordered = {}
	for cmdkey, cmd in pairs(AutoBand.cmd) do
		cmd.key = cmdkey
		table.insert(AutoBand.cmd_ordered, cmd)
	end
	table.sort(AutoBand.cmd_ordered, function(cm1, cm2) return cm1.print_id < cm2.print_id end)
	AB_util.debug(AutoBand.cmd_ordered)

	local motd = "AutoBand v" .. AB_const.VERSION .. " loaded. Type /autoband"
	if (AutoBand.has_shortslash) then
		motd = motd .. " or /ab"
	end
	motd = motd .. " for instructions"
	AB_util.print(motd)

	-- register our icon
	LayoutEditor.RegisterWindow("AutoBandMapIcon", L"AutoBand", L"AutoBand Button", false, false, true, nil)
end


function AutoBand.update(elapsed)
	AutoBand.elapsed = AutoBand.elapsed + elapsed
	if (AutoBand.elapsed > AB_const.HEARTBEAT) then
		local e = AutoBand.elapsed
		AutoBand.elapsed = 0		-- just in case something goes wrong inside auto_kick()
		AutoBand.auto_kick(e)
	end

	if (AutoBand.cmd_current == nil) then
		if (#AutoBand.cmd_queue > 0) then
			AutoBand.cmd_current = table.remove(AutoBand.cmd_queue, 1)		-- remove first cmd
		else
			return				-- no current and nothing to dequeue, just return
		end
	end
	AutoBand.cmd_current.ticks = AutoBand.cmd_current.ticks - 1
	if (AutoBand.cmd_current.ticks <= 0) then
		if (AutoBand.debugon or AutoBand.gizilon) then
			AB_util.debug(tostring(AutoBand.cmd_current.cmd))
		end
		SendChatText (towstring(AutoBand.cmd_current.cmd), L"")
		local cmd_tmp = AutoBand.cmd_current
		AutoBand.cmd_current = nil	-- for security clear cmd_current before callback
		if (cmd_tmp.callback) then
			cmd_tmp.callback(cmd_tmp)
		end
	end
end


-- enqueues a string to be processed by "AutoBand.update()"
function AutoBand.enqueue_command(cmd, ticks, callback)
	ticks = ticks or AB_const.DEFAULT_CMD_TICKS
	table.insert(AutoBand.cmd_queue, {["cmd"] = cmd, ["ticks"] = ticks, ["callback"] = callback})
end

function AutoBand.enqueue_kick(player_name, msg_reason)
	if (msg_reason) then
		AutoBand.enqueue_command("/tell " .. tostring(player_name) ..  " [AutoBand] You were kicked from the wb since " .. msg_reason  )
	end
	AutoBand.enqueue_command(L"/kick " .. player_name)
	AB_util.print(L"Kicking " .. player_name)
end


function AutoBand.has_permission()
	return GameData.Player.isWarbandAssistant or GameData.Player.isGroupLeader
end


-- returns a AB_wb object either from a warband, scenario or dumped_wb
function AutoBand.get_wb()
	if (AutoBand.debugon and AutoBand.saved.dumped_wb ~= nil) then
		AB_util.debug("Getting dump " .. AutoBand.saved.default_dump .." size " .. AB_util.size_wb(AutoBand.saved.dumped_wb.group))
		return AB_wb:new(AutoBand.saved.dumped_wb)		-- returns dumped wb
	end

	local wb = AB_wb:new()
	wb:load_from_wbdata()
	return wb
end


-- ####### / Commands #######

function AutoBand.parse_cmd(msg)
	msg = msg:lower()
	local args = StringSplit(msg)
	cmd = table.remove(args, 1)
	if (AutoBand.cmd[cmd] ~= nil) then
		AutoBand.cmd[cmd].f(args)
	else
		AB_util.print("AutoBand unknown command: " .. msg)
	end
end

function AutoBand.cmd_usage(args)
	local ab = "/autoband "
	if (AutoBand.has_shortslash) then
		ab = "/ab "
	end
	AB_util.print("[Autoband commands]")
	--for cmdkey, cmd in pairs(AutoBand.cmd) do
	for _, cmd in ipairs(AutoBand.cmd_ordered) do
		if (cmd.help ~= "" or AutoBand.debugon) then
			AB_util.print(ab .. cmd.key .. " - " .. cmd.help)
		end
	end
end

function AutoBand.cmd_show(args)
	AB_util.print("[AutoBand settings]")
	AB_util.print("Auto kick offline players: " .. tostring(AutoBand.saved.autokick_enabled))
	AB_util.print("Auto kick timeout: " .. tostring(AutoBand.saved.autokick_period))
	AB_util.print("Minimum rank requirement: " .. tostring(AutoBand.saved.min_rank))
	AB_util.print("Default template: " .. AutoBand.saved.default_template)
	if (AutoBand.debugon) then
		AB_util.print("Data dumped: " .. tostring(AutoBand.saved.dumped_wb ~= nil))
		AB_util.print("Debug mode: true")
	end
end

function AutoBand.cmd_debug(args)
	AutoBand.debugon = not AutoBand.debugon
	AB_util.print("Debug mode: " .. tostring(AutoBand.debugon))
end

function AutoBand.cmd_flag_autokick(args)
	AutoBand.saved.autokick_enabled = not AutoBand.saved.autokick_enabled
	AB_util.print("Auto kick offline players: " .. tostring(AutoBand.saved.autokick_enabled))
end

function AutoBand.cmd_dump(args)	-- give a name to your dump
	if (args[1] == nil) then
		AB_util.print("[error] Missing argument")
		return
	end
	local wb = AB_wb:new()
	wb:load_from_wbdata()
	AutoBand.saved.dumped_wb = wb 
	AutoBand.saved.default_dump = args[1]
	AutoBand.saved.dumps[args[1]] = wb 
	AB_util.print("Dump complete")
end

function AutoBand.cmd_cleardumps(args)
	AutoBand.saved.dumped_wb = nil
	AutoBand.saved.dumps = {}
	AutoBand.saved.default_dump = nil
end

function AutoBand.cmd_kick_offline(args)
	if (not AutoBand.has_permission()) then
		AB_util.print("[error] You need to be a group assistant or leader")
		return
	end
	local players = AutoBand.get_offliners()
	if (#players > 0) then
		AB_util.print("Kicking offliners ...")
		for _, player in ipairs(players) do
			AutoBand.enqueue_kick(player.name)
		end
		
	end
	AB_util.print("Kickoff complete")
end


function AutoBand.cmd_list_offline(args)
	local str = ""
	local players = AutoBand.get_offliners()
	if (#players > 0) then
		for _, player in ipairs(players) do
			str = str .. tostring(player.name) .. " "
		end
		AB_util.print(AB_const.HEADER_MARGIN ..  " Offline players" .. AB_const.HEADER_MARGIN .. " (#" .. #players .. ")")
		AB_util.print(str)
	end
end


function AutoBand.cmd_list_rank(args)
	local str = ""
	local players = AutoBand.get_low_rank()
	if (#players > 0) then
		table.sort(players, function(p1,p2) return p1.level > p2.level end)
		for _, player in ipairs(players) do
			str = str .. tostring(player.name) .. "(r" .. player.level .. ") "
		end
		AB_util.print(AB_const.HEADER_MARGIN .. " Players with rank < " .. AutoBand.saved.min_rank .. " " .. AB_const.HEADER_MARGIN .. " (#" .. #players .. ")")
		AB_util.print(str)
	end
end


function AutoBand.cmd_list_zone(args)
	local wb_obj = AutoBand.get_wb()
	if (wb_obj.player_count == 0) then
		AB_util.debug("No wb data found")
		return
	end
	local players_by_zid = AutoBand.get_players_zone(wb_obj)
	local num_valid_zones = AB_util.size_table(players_by_zid["valid_as"])
	if (players_by_zid["valid_as"] and num_valid_zones > 0) then
		AB_util.print(AB_const.HEADER_MARGIN .. " Leader's and assistants' zones " .. AB_const.HEADER_MARGIN)
		for zid, as_zone in pairs(players_by_zid["valid_as"]) do
			local num_assistants = AB_util.size_table(as_zone)
			local _num_assistants = AB_util.size_table(players_by_zid["valid_as"][zid])
			AB_util.debug("1st " .. zid .. " " ..num_assistants .. " =? " .. _num_assistants)
			local str = ""
			local num_players = AB_util.size_table(players_by_zid["valid_p"][zid]) -- num_assistants + 
			local zone_name = GetZoneName(zid)
			if (zid == 0) then
				zone_name = "Offline"
			end	
			str = str .. tostring(zone_name) .. " (#" .. num_players .. ") "
			for _, player in ipairs(as_zone) do
				if (player.isGroupLeader) then
					str = str .. tostring(player.name) .. "(" .. AB_const.ABBREVATIONS["LEADER"] .. ") "
				else
					str = str .. tostring(player.name) .. "(" .. AB_const.ABBREVATIONS["ASSISTANT"] .. ") "
				end
			end
			AB_util.print(str)
		end
	end
	local num_invalid_zones = AB_util.size_table(players_by_zid["other"])
	if (players_by_zid["other"] and num_invalid_zones > 0) then	
		AB_util.print(AB_const.HEADER_MARGIN .. " " .. AB_const.LABEL_NOTSAMEZONE .. " " .. AB_const.HEADER_MARGIN)
		for zid, other_zone in pairs(players_by_zid["other"]) do
			local str = ""
			local num_players = AB_util.size_table(other_zone)
			AB_util.debug("2nd " .. zid .. " " .. num_players .. " =? " .. #other_zone)
			str = str .. tostring(GetZoneName(zid)) .. " (#" .. #other_zone .. ") "
			for _, player in ipairs(other_zone) do
				str = str .. tostring(player.name) .. " "
			end
		--	str = str .. "\n"
			AB_util.print(str)
		end
		
		
	end
end	

function AutoBand.cmd_kick_notinzone(args)
	if (not AutoBand.has_permission()) then
		AB_util.print("[error] You need to be a group assistant or leader")
		return
	end

	local wb_obj = AutoBand.get_wb()
	if (wb_obj.player_count == 0) then
		AB_util.debug("No wb data found")
		return
	end
	local zone_name = GetZoneName
	local players_by_zid = AutoBand.get_players_zone(wb_obj)
	local num_valid_zones = AB_util.size_table(players_by_zid["valid_as"])
	if (players_by_zid["valid_as"] and num_valid_zones > 0) then
		local valid_zone_str = ""
		for zid, as_zone in pairs(players_by_zid["valid_as"]) do
			if (zid ~= 0) then
				-- offline is not reported as a valid zone
				valid_zone_str = valid_zone_str .. tostring(zone_name(zid)) .. " "
			end
		end
		local num_invalid_zones = AB_util.size_table(players_by_zid["other"])
		if (players_by_zid["other"] and num_invalid_zones > 0) then	
			AB_util.print("Kicking " .. AB_const.LABEL_NOTSAMEZONE)
			for zid, other_zone in pairs(players_by_zid["other"]) do
				if (zid ~= 0) then	-- i dont want to kick offline players
					for _, player in ipairs(other_zone) do
						AutoBand.enqueue_kick(player.name, "you are not in one of the required zones {" .. valid_zone_str .. "}")
					end
				end
			end	
			
		end
	end
	AB_util.print("Zone kick complete")
end

function AutoBand.cmd_rank(args)
	if (args[1] == nil) then
		AB_util.print("[error] Missing argument")
		return
	end
	local n = tonumber(args[1])
	if (n < 0 or n >40) then
		AB_util.print("[error] Invalid rank value: " .. n)
		return
	end
	AutoBand.saved.min_rank = math.floor(n)
	AB_util.print("Minimum rank requirement: " .. tostring(AutoBand.saved.min_rank))
end

function AutoBand.cmd_kick_rank(args)
	if (not AutoBand.has_permission()) then
		AB_util.print("[error] You need to be a group assistant or leader")
		return
	end
	local players = AutoBand.get_low_rank()
	if (#players > 0) then
		AB_util.print("Kicking players with rank < " .. AutoBand.saved.min_rank)
		for _, player in ipairs(players) do
			AutoBand.enqueue_kick(player.name, "you dont meet the rank requirement ("  .. AutoBand.saved.min_rank .. ")")
		end
	end
	AB_util.print("Rank < " .. AutoBand.saved.min_rank .." kick complete")
end

function AutoBand.cmd_organize(args)
	if (AutoBand.has_permission() or AutoBand.debugon) then
		AutoBand.saved.autokick_enabled = false	-- safe measure to prevent changes during auto_organize
		if (args[1] ~= nil) then
			AB_org.auto_organize(AutoBand.saved.templates[args[1]])
		else
			AB_org.auto_organize(AutoBand.saved.templates[AutoBand.saved.default_template])
		end
		AutoBand.saved.autokick_enabled = true	-- NOTE: we could still be already inside auto_kick()
	else
		AB_util.print("[error] You need to be a group assistant or leader")
	end
end

function AutoBand.cmd_autokick_timeout(args)
	if (args[1] == nil) then
		AB_util.print("[error] Missing argument")
		return
	end
	local n = tonumber(args[1])
	if (n < 0) then
		AB_util.print("[error] Invalid timeout value: " .. n)
		return
	end
	AutoBand.saved.autokick_period = math.floor(n)
	AB_util.print("Auto kick timeout: " .. tostring(AutoBand.saved.autokick_period))
end

function AutoBand.cmd_mode(args)
	if (args[1] == nil) then
		AB_util.print("[error] Missing argument")
		return
	end
	local n = tonumber(args[1])
	if (n < 0 or n > 2) then
		AB_util.print("[error] Invalid distribution algorithm: " .. n)
		return
	end
	AutoBand.saved.org_algo_mode = math.floor(n)
	AB_util.print("Distribution algorithm mode: " .. tostring(AutoBand.saved.org_algo_mode) .. AB_const.ALGO_MODE[AutoBand.saved.org_algo_mode])
end

function AutoBand.cmd_list_template(args)
	AB_util.print("Default template " .. AutoBand.saved.default_template)

	for tname in pairs(AutoBand.saved.templates) do
		AB_util.print(tname)
	end
end

function AutoBand.cmd_save_template(args)
	if (args[1] == nil) then
		AB_util.print("[error] Missing argument")
		return
	end
	AutoBand.saved.templates[args[1]] = AB_template.from_wb(AutoBand.get_wb())
	AB_util.print("Template " .. args[1].. " saved")
end

function AutoBand.cmd_apply_template(args)
	if (args[1] == nil) then
		AB_util.print("[error] Missing argument")
		return
	end
	if (AutoBand.saved.templates[args[1]] == nil) then
		AB_util.print("[error] Template " .. args[1] .. " does not exist")
		return
	end
	AB_org.auto_organize(AutoBand.saved.templates[args[1]])
	AB_util.print("Template " .. args[1].. " loaded")
end

function AutoBand.cmd_default_template(args)
	if (#args > 0) then
		if (AutoBand.saved.templates[args[1]] == nil) then
			AB_util.print("[error] Template " .. args[1] .. " does not exist")
			return
		end
		AutoBand.saved.default_template = args[1]
	else
		AutoBand.saved.default_template = AB_const.EMPTY_TEMPLATE
	end
	AB_util.print("Default template set: " .. AutoBand.saved.default_template)
end

function AutoBand.cmd_toggle_gui(args)
	if (AutoBandWindow.showing()) then
		AutoBandWindow.Hide()
	else
		AutoBandWindow.Show()
	end
end

function AutoBand.cmd_toggle_icon(args)
	AutoBandWindow.toggle_mapicon()
end

-- ####### [end] / Commands #######

-- ####### [start] / Debug Purposes Commands #######

function AutoBand.cmd_list_dump(args)
--	if (AutoBand.saved.default_dump ~= nil) then
--		AB_util.print("[AutoBand] default dump " .. AutoBand.saved.default_dump)
	if (AutoBand.saved.dumped_wb ~= nil) then
		AB_util.print("[AutoBand] default dump " .. AutoBand.saved.default_dump)
	end
	for tname in pairs(AutoBand.saved.dumps) do
		AB_util.print(tname)
	end
end

function AutoBand.cmd_load_dump(args)
	if (#args > 0) then
		if (AutoBand.saved.dumps[args[1]] == nil) then
			AB_util.print("[error] Dump " .. args[1] .. " does not exist")
			return
		end
		AutoBand.saved.default_dump = args[1]
		AutoBand.saved.dumped_wb = AutoBand.saved.dumps[AutoBand.saved.default_dump]
	end
	AB_util.print("[AutoBand] Default dump set: " .. AutoBand.saved.default_dump)
end

function AutoBand.cmd_dump_from_template(args)
	if(AutoBand.debugon) then
		if (#args > 1) then
			local template_name = args[1]
			local dump_name = args[2]
			if (AutoBand.saved.templates[template_name] == nil) then
				AB_util.print("[error] Template " .. template_name .. " does not exist")
				return
			end
			AutoBand.saved.dumped_wb = AutoBand.get_fakewb(AutoBand.saved.templates[template_name])
			AutoBand.saved.dumps[dump_name] = AutoBand.saved.dumped_wb
			AutoBand.saved.default_dump = dump_name
		end
	end
end

-- ####### [end] / Debug Purposes Commands #######

function AutoBand.auto_kick(elapsed)
	-- check for flag, permission and debugmode
	if (not AutoBand.saved.autokick_enabled or not AutoBand.has_permission() or AutoBand.debugon) then
		return
	end
	local wb_obj = AutoBand.get_wb()
	wb_obj:foreach_player(
		function(gid, player)
			if (player.online) then
				if (AutoBand.offliners_countdown[player.name] ~= nil) then -- if the player was offline before and he/she came back
					AB_util.debug(L"User " .. player.name .. L" became online")
					AutoBand.offliners_countdown[player.name] = nil
				end
			else
				if (AutoBand.offliners_countdown[player.name] == nil) then -- if the player was not offline before and he/she went offline
					AB_util.debug(L"User " .. player.name .. L" became offline")
					AutoBand.offliners_countdown[player.name] = AutoBand.saved.autokick_period * AB_const.HEARTBEAT
				else
					AutoBand.offliners_countdown[player.name] = AutoBand.offliners_countdown[player.name] - elapsed
					if (AutoBand.offliners_countdown[player.name] < 0) then -- you must kick the player
						AB_util.print(L"Kicking offline " .. player.name)
						AutoBand.enqueue_command(L"/kick " .. player.name)
						AutoBand.offliners_countdown[player.name] = nil
					end
				end
			end
		end
	)
end


-- ####### [start] / Get Functions #######

-- return a list of players < AutoBand.saved.min_rank
function AutoBand.get_low_rank(wb_obj)
	if (not wb_obj) then
		wb_obj = AutoBand.get_wb()
	end
	local list = {}
	wb_obj:foreach_player(
		function(gid, player)
			if (player.level < AutoBand.saved.min_rank) then
				table.insert(list, player)
			end
		end
	)
	return list
end

-- return a list of offline players
function AutoBand.get_offliners(wb_obj)
	if (not wb_obj) then
		wb_obj = AutoBand.get_wb()
	end
	local list = {}
	wb_obj:foreach_player(
		function(gid, player)
			if (player.zoneNum == 0 or not player.online) then
				table.insert(list, player)
			end
		end
	)
	return list
end

-- return a list of players by zone : assumes that the player is in a warband
function AutoBand.get_players_zone(wb_obj)
	local zids = {["valid_as"]= {}, ["valid_p"]= {}}
	local leader_zone = nil
	wb_obj:foreach_player(
		function(gid, player)
			if (player.isGroupLeader or player.isAssistant) then
				if (zids["valid_as"][player.zoneNum] == nil) then
					zids["valid_as"][player.zoneNum] = {}
				end
				if (player.isGroupLeader) then
					leader_zone = player.zoneNum
					table.insert(zids["valid_as"][player.zoneNum], 1, player)
				else
					table.insert(zids["valid_as"][player.zoneNum], player)
				end
			end
		end
	)
	if (leader_zone) then
		zids["other"]= {}
		wb_obj:foreach_player(
			function(gid, player)
	--			if (player.online) then
					if (zids["valid_as"][player.zoneNum]) then
						if (zids["valid_p"][player.zoneNum] == nil) then
							zids["valid_p"][player.zoneNum] = {}
						end
						table.insert(zids["valid_p"][player.zoneNum], player)
					else
						if (zids["other"][player.zoneNum] == nil) then
							zids["other"][player.zoneNum] = {}
						end
						table.insert(zids["other"][player.zoneNum], player)
					end
	--			end
			end
		)
	else 
		AB_util.print("[ERROR] Leader cannot be found, the warband is BUGGED, reform a warband")
	end
	return zids
end



-- [DEBUG] returns a AB_wb object either from a warband, scenario or dumped_wb
function AutoBand.get_fakewb(template)
	local wb = AB_wb:new()
	wb:set_groups()
	local num_players = 1
	for gid, tgroup in ipairs(template) do
		for j, tplayer in ipairs(tgroup) do
			wb:add_to_gid(tplayer.role, num_players, gid)
			num_players = num_players + 1
		end
	end
	wb:recalc_counters()
	wb:print()
	return wb
end

-- ####### [end] / Get Functions #######

--[[
function AutoBand.checkBuffs()
	AB_util.debug("checking buffs")
	for i, target in ipairs(AutoBand.buffTargets) do
		local buffs = GetBuffs(target)
		if ( buffs == nil) then
			AB_util.debug("Player " .. target .. " without buff ")
		else
			for bid, buff in pairs(buffs) do
				AB_util.debug("Player " .. target .. " buff " .. bid)
				AB_util.debug(buff)
			end
		end
	end
end
]]--