-- ab_const.lua
-- The constants

AB_const = {}

AB_const.VERSION = "0.88"

AB_const.GROUP_SIZE = 6
AB_const.MAX_WB_GROUPS = 4
AB_const.HEARTBEAT = 60				-- period in seconds
AB_const.KICK_PERIOD = 5			-- number of HEARTBEATs smo can go offline, after that they will be kicked off
AB_const.DEFAULT_CMD_TICKS = 1			-- default number of ticks a command must wait to be executed
AB_const.AUTOKICK = true
AB_const.EMPTY_TEMPLATE = "<Automatic>"		-- name of the automatic template
AB_const.EMPTY_WB_TEMPLATE = {[1] = {}, [2] = {}, [3] = {}, [4] = {}}


AB_const.MINIMUM_RANK = 37			-- minimum rank required in the wb
AB_const.MAXIMUM_RANK = 40

AB_const.ABBREVATIONS = {["LEADER"] = "L", ["ASSISTANT"] = "A"} 

AB_const.HEALER = "healer"
AB_const.TANK = "tank"
AB_const.MDPS = "mdps"
AB_const.RDPS = "rdps"

AB_const.HEADER_MARGIN = "==="

AB_const.CAREERLINE_MAP = {
	[GameData.CareerLine.ENGINEER] = AB_const.RDPS,
	[GameData.CareerLine.BRIGHT_WIZARD] = AB_const.RDPS,
	[GameData.CareerLine.SORCERER] = AB_const.RDPS,
	[GameData.CareerLine.SQUIG_HERDER] = AB_const.RDPS,
	[GameData.CareerLine.MAGUS] = AB_const.RDPS,
	[GameData.CareerLine.SHADOW_WARRIOR] = AB_const.RDPS,

	[GameData.CareerLine.WITCH_ELF] = AB_const.MDPS,
	[GameData.CareerLine.WHITE_LION] = AB_const.MDPS,
	[GameData.CareerLine.SLAYER] = AB_const.MDPS,
	[GameData.CareerLine.WITCH_HUNTER] = AB_const.MDPS,
	[GameData.CareerLine.CHOPPA] = AB_const.MDPS,
	[GameData.CareerLine.MARAUDER] = AB_const.MDPS,

	[GameData.CareerLine.IRON_BREAKER] = AB_const.TANK,
	[GameData.CareerLine.CHOSEN] = AB_const.TANK,
	[GameData.CareerLine.BLACK_ORC] = AB_const.TANK,
	[GameData.CareerLine.KNIGHT] = AB_const.TANK,
	[GameData.CareerLine.SWORDMASTER] = AB_const.TANK,
	[GameData.CareerLine.BLACKGUARD] = AB_const.TANK,

	[GameData.CareerLine.ZEALOT] = AB_const.HEALER,
	[GameData.CareerLine.WARRIOR_PRIEST] = AB_const.HEALER,
	[GameData.CareerLine.RUNE_PRIEST] = AB_const.HEALER,
	[GameData.CareerLine.ARCHMAGE] = AB_const.HEALER,
	[GameData.CareerLine.SHAMAN] = AB_const.HEALER,
	[GameData.CareerLine.DISCIPLE] = AB_const.HEALER
}


AB_const.FAKE_PLAYERS = {[AB_const.HEALER]= {["careerLine"]= GameData.CareerLine.ZEALOT,["online"]= true}, 
			[AB_const.TANK]	= {["careerLine"]= GameData.CareerLine.CHOSEN,["online"]= true},
			[AB_const.MDPS]	= {["careerLine"]= GameData.CareerLine.CHOPPA,["online"]= true},
			[AB_const.RDPS]	= {["careerLine"]= GameData.CareerLine.SORCERER,["online"]= true}}


AB_const.ROLE_CATEGORIES = { [AB_const.HEALER] = 1, [AB_const.TANK] = 2, [AB_const.MDPS] = 3, [AB_const.RDPS] = 4 }


-- AB_template
AB_const.MODE_SPREAD = 1
AB_const.MODE_AGGREGATE = 2
AB_const.ALGO_MODE = {[AB_const.MODE_SPREAD] = "Spread categories", [AB_const.MODE_AGGREGATE] = "Aggregate categories"}

AB_const.DEFAULT_ORG_ALGO = AB_const.MODE_SPREAD	-- distributing equally
AB_const.DEFAULT_ORG_ROLE = AB_const.HEALER

AB_const.LOOP_MAX = 100


-- AutoBandWindow
AB_const.TABS_TEMPLATE = 1
AB_const.TABS_TOOLS = 2
AB_const.TABS_CONFIG = 3

--AB_const.TABS_HISTORY = 4

-- AutoBandWindowTemplate
AB_const.LABEL_EMPTY = "---"
AB_const.LABEL_ROLE_COLORS = { [AB_const.HEALER] = {0, 210, 0}, [AB_const.TANK] = {0, 0, 210}, [AB_const.MDPS] = {210, 0, 0}, [AB_const.RDPS] = {210, 0, 210}, 
				[AB_const.LABEL_EMPTY] = {210, 210, 210} }
AB_const.LABEL_ROLE_CATEGORIES = { [AB_const.HEALER] = 1, [AB_const.TANK] = 2, [AB_const.MDPS] = 3, [AB_const.RDPS] = 4, [AB_const.LABEL_EMPTY] = 5 }
AB_const.LABEL_ROLE_CATEGORIES_REV = { AB_const.HEALER, AB_const.TANK, AB_const.MDPS, AB_const.RDPS, AB_const.LABEL_EMPTY }

AB_const.CURRENT_WB = "<Warband>"

-- AutoBandWindowConfig
AB_const.MAX_KICKOFF_TIME = 9

AB_const.LABEL_NOTSAMEZONE = "Players not in leader's / assistants' zone"


-- AutoBandWindowTools
AB_const.KICK_OFFLINE = 1
AB_const.KICK_ZONE = 2
AB_const.KICK_RANK =3
AB_const.LABEL_KICK = { [AB_const.KICK_OFFLINE]= "offline", [AB_const.KICK_ZONE] = "not in leader's / assistants' zone", [AB_const.KICK_RANK] = "low rank" }
AB_const.KICK_FUNC_DEFAULT = { [AB_const.KICK_OFFLINE]= true, [AB_const.KICK_ZONE] = false, [AB_const.KICK_RANK] = false }

