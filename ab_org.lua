-- ab_org.lua

AB_org = {}

-- edge colors
AB_org.EC_CLEAR = 0
AB_org.EC_VISITED = 1
AB_org.EC_DEADEND = -1
AB_org.EC_CYCLE = 2

-- automatically tries to organize the wb by moving and swappinh players
function AB_org.auto_organize(template)
	-- get the current warband
	local wb_obj = AutoBand.get_wb()
	if (wb_obj == nil) then
		AB_util.print("[error] no data")
		return
	end

	local bins_role = {}	
	local roles_not_taken = {}
	for cat_key, cat in pairs(AB_const.ROLE_CATEGORIES) do
		bins_role[cat] = {}
		roles_not_taken[cat_key] = {}
	end

	-- create new distribution
	local wb_dist = {}
	local players_not_placed = {}
	local grp_out = {}
	for i = 1, wb_obj.MAX_GROUPS do
		wb_dist[i] = {}
		grp_out[i] = {}
		players_not_placed[i] = {}
	end

	-- sort players by their roles
	wb_obj:foreach_player(
		function(gid, player, pid)
			table.insert(bins_role[AB_const.ROLE_CATEGORIES[player.role]], {["role"] = player.role, ["career"] = player.careerLine })
			players_not_placed[gid][pid] =  player	--tostring(player.name)
		end
	)

	for i, bin in ipairs(bins_role) do
		if (AutoBand.debugon) then
			AB_util.print(i .. "  " .. #bin)
		end
		-- sort by careerLine
		table.sort(bin, function (a, b) return a.career > b.career end)
	end

	if (AutoBand.debugon) then
		AB_util.debug(bins_role)
	end
	AB_template.match(wb_obj, wb_dist, roles_not_taken, bins_role, template)

	if (AutoBand.debugon) then
		AB_util.debug("After match")
		AB_util.debug("Roles " .. AB_util.size_wb(roles_not_taken))
		AB_util.debug("Players " .. AB_util.size_wb(players_not_placed))
		AB_util.debug("Bins " .. AB_util.size_wb(bins_role))
		AB_util.debug("Expected group distribution1")
		AB_util.print_wb(wb_dist)
--		AB_util.debug("Expected group distribution2")
--		AB_util.print_wb(roles_not_taken)
		AB_util.debug("Current group distribution2")
		wb_obj:print()
--		return
	end

	-- 1. remove players that wont be moved and their roles taken 
	for fromgid, group in ipairs(players_not_placed) do
		local group_size = #group
		for pid = group_size, 1, -1 do		-- loop backwards because we remove items
			local player = group[pid]
			local prole_size = #roles_not_taken[player.role]
			for i = prole_size, 1, -1 do
				local rgid = roles_not_taken[player.role][i]
				if (rgid == fromgid) then	-- if we found a role for this player in the same group
					table.remove(roles_not_taken[player.role], i)
					table.remove(group, pid)
					break
				end
			end
		end
	end
	-- 2. find where the players should be moved in "grp_out"
	for fromgid, group in ipairs(players_not_placed) do
		local group_size = #group
		for pid = group_size, 1, -1 do		-- loop backwards because we remove items
			local player = group[pid]
			local togid = table.remove(roles_not_taken[player.role])
			table.insert(grp_out[fromgid], {["name"]=player.name, ["to"]=grp_out[togid], ["togid"]=togid, ["fromgid"]=fromgid, ["color"]=AB_org.EC_CLEAR})
			table.remove(group, pid)
		end
	end

	if (AutoBand.debugon) then
		AB_org.verify_org(roles_not_taken, players_not_placed)

	end

	-- find all move and swap operations
	local move_list = {}
	local swap_list = {}
	for i = 1, wb_obj.MAX_GROUPS do
		repeat
			local path = {}
			local ret = AB_org.search(path, move_list, grp_out[i])
			if (ret >= 2) then
				-- found cycle, add to swap list and retry the same root node
				table.insert(swap_list, path)
			end
		until (ret == 0)
	end

	if (AutoBand.debugon) then
		AB_util.print("move list")
		for _, mv in pairs(move_list) do
			AB_util.print(tostring(mv.name) .. " " .. mv.fromgid .. " -> " .. mv.togid)
		end
		AB_util.print("swap list")
		for j, cycle in pairs(swap_list) do
			AB_util.print("cycle " .. j)
			for i = 2, #cycle do
				AB_util.print(tostring(cycle[1].name) .. " <-> " .. tostring(cycle[i].name))
			end
		end
	end

	-- process swaps
	for _, cycle in ipairs(swap_list) do
		for i = 2, #cycle do
			AB_org.swap_players(wb_obj.group, cycle[1], cycle[i])
		end
	end

	-- process moves: brute force mode, loop until all moves are done
	-- we can inprove this and create a grp_in for incoming edges, so we know which groups can accept
	local i = #move_list
	while (i > 0) do
		local v = move_list[i]
		if (#wb_obj.group[v.togid] < AB_const.GROUP_SIZE) then
			AB_org.move_player(wb_obj.group, v)
			-- remove and add him to new logical group
			if (not AutoBand.debugon) then	-- in debug mode this is already done
				table.insert(wb_obj.group[v.togid], table.remove(wb_obj.group[v.fromgid]))
			end
			table.remove(move_list, i) -- remove this v from move_list
--			move_list[i] = nil
		end
		i = i - 1
		if (i == 0) then
			i = #move_list
		end
	end

	if (AutoBand.debugon) then
		AutoBand.saved.templates["debug"] = AB_template.from_wb(wb_obj)
		AutoBandWindowTemplate.refresh_gui()
		if (AB_org.verify_result(wb_obj, wb_dist)) then
			AB_util.print("The warband is perfectly organized")
		else
			AB_util.print("The warband can not be organized the way expected")
		end
	else
		if (AutoBand.gizilon) then
			AutoBand.saved.templates["#expected"] = AB_template.from_wb(wb_obj)
			AutoBandWindowTemplate.refresh_gui()
		end
		AutoBand.enqueue_command("/wb [AutoBand] Done")
	end
end

function AB_org.verify_org(roles_not_taken, players_not_placed)
	AB_util.debug(" Roles " .. AB_util.size_wb(roles_not_taken))
	if (AB_util.size_wb(roles_not_taken) > 0) then
		AB_util.debug("There is a role that is not allocated ")
		for gid, group in ipairs(roles_not_taken) do
			for rid, role in pairs(group) do
				AB_util.debug(role .. " at g" .. gid .. " is not allocated ")
			end
		end
	end

	AB_util.debug(" Players " .. AB_util.size_wb(players_not_placed))
	if (AB_util.size_wb(players_not_placed) > 0) then
		AB_util.debug("There is a player that does not have a place ")
		for gid, group in ipairs(players_not_placed) do
			for pname, player in pairs(group) do
				AB_util.debug(tostring(pname) ..  " at g" .. gid .. " is not in a correct group")
			end
		end
	end


end

function AB_org.verify_result(wb_obj, roles_new)
	-- player colors
	local wb_stats = {}
	for i = 1, wb_obj.MAX_GROUPS do
		wb_stats[i] = {}
		for j = 1, AB_const.GROUP_SIZE do
			table.insert(wb_stats[i], #wb_stats[i]+1, {["role_taken"]=false, ["place_taken"]=false})
		end
	end

	-- mark correct players that can not move
	wb_obj:foreach_player(
		function (gid, player, pid)
			for rid, role in ipairs(roles_new[gid]) do
				if ((not(wb_stats[gid][rid].role_taken) and not(wb_stats[gid][pid].place_taken)) and player.role == role) then
					wb_stats[gid][rid].role_taken = true	-- set the players place final
					wb_stats[gid][pid].place_taken = true	-- set this role 
					break
				end
			end
		end
	)

	wb_obj:foreach_player(
		function (gid, player, pid)
			if (not(wb_stats[gid][pid].role_taken)) then
				AB_util.debug(roles_new[gid][pid].role .. " at g" .. gid .. " is not allocated ")
				return false -- error
			end
			if (not(wb_stats[gid][pid].place_taken)) then
				AB_util.debug(tostring(player.name) ..  " at g" .. gid .. " is not in a correct group")
				return false -- error
			end
		end
	)

	return true
end


-- enqueues a swap player command
function AB_org.swap_players(wb, p1, p2)
	if (AutoBand.debugon) then
		for i, player1 in pairs(wb[p1.fromgid]) do
			if (tostring(player1.name) == tostring(p1.name)) then
				for j, player2 in ipairs(wb[p2.fromgid]) do
					if (player2.name == p2.name) then
						AB_util.print("Swapping " .. tostring(player1.name) .. " at g" .. p1.fromgid .. " with " .. tostring(player2.name) .. " at g" .. p2.fromgid)
						wb[p1.fromgid][i], wb[p2.fromgid][j] = wb[p2.fromgid][j], wb[p1.fromgid][i]
						p1.fromgid, p2.fromgid = p2.fromgid, p1.fromgid
						return	-- players found and swaped
					end
				end
			end
		end
	end
	AutoBand.enqueue_command(L"/warbandswap " .. p1.name .. L" " .. p2.name)
end


-- enqueues a move player command
function AB_org.move_player(wb, p)
	if (AutoBand.debugon) then
		for i, player in ipairs(wb[p.fromgid]) do
			if (player.name == p.name) then
				AB_util.print("Moving " .. tostring(p.name) .. " at g" .. p.fromgid.. " to g" .. p.togid)
				table.insert(wb[p.togid], table.remove(wb[p.fromgid], i))
				p.fromgid = p.togid
				return	-- player found and moved
			end
		end
	end
	AutoBand.enqueue_command(L"/warbandmove " .. p.name .. L" " .. towstring(p.togid), 3)	-- ticks for a move
end


-- close your eyes
-- return 2 or 3 if a cycle was found, param "cycle" contains the edges
-- return 0 if all edges were visited
function AB_org.search(cycle, moves, node)
	-- check for a visited edge (that means a cycle)
	for _,edge in pairs(node) do
		if (edge.color == AB_org.EC_VISITED) then
			edge.color = AB_org.EC_CYCLE		-- mark the start of the cycle
			return 2
		end
	end

	-- for every outgoing edge
	for _,edge in pairs(node) do
		if (edge.color == AB_org.EC_CLEAR) then	-- if edge is clear
			edge.color = AB_org.EC_VISITED	-- mark as visited
			local ret = AB_org.search(cycle, moves, edge.to)
			if (ret == 2) then
				-- processing the cycle
				table.insert(cycle, edge)	-- add edge to cycle
				if (edge.color == AB_org.EC_VISITED) then
					edge.color = AB_org.EC_CYCLE
					return 2		-- still in the middle of the cycle
				else
					return 3		-- found the start of cycle, cycle closed
				end
			elseif (ret == 3) then
				-- a cycle was found and closed, clear upper edges
				edge.color = AB_org.EC_CLEAR	-- clear edge
				return 3
			elseif (ret == 0) then
				-- found a dead end, mark it and add a move
				edge.color = AB_org.EC_DEADEND
				table.insert(moves, edge)
				-- try other edges, don't return to parent
			end
		end
	end
	return 0						-- nothing else to visit
end
-- you can open now