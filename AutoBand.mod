<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<UiMod name="AutoBand" version="0.8.8" date="2020-05-27T16:03:45Z" >
	<Author name="Differential and Eowoyn" email="no@way" />
	<VersionSettings gameVersion="1.3.3" windowsVersion="1.0" savedVariablesVersion="1.0" />
	<Description text="Automatic warband manager" />

	<WARInfo>
	    <Categories>
	        <Category name="GROUPING" />
	    </Categories>
	</WARInfo>

	<Dependencies>
		<Dependency name="LibSlash"/>
		<Dependency name="EASystem_Strings"/>
		<Dependency name="EASystem_WindowUtils"/>
	</Dependencies>

	<Files>
		<File name="AB_const.lua"/>
		<File name="AutoBand.lua"/>
		<File name="AB_org.lua"/>
		<File name="AB_util.lua"/>
		<File name="AB_wb.lua"/>
		<File name="AB_template.lua"/>
		
		<File name="AutoBandWindowTemplate.xml" />
		<File name="AutoBandWindowConfig.xml" />
		<File name="AutoBandWindowTools.xml" /> 
	<!--	<File name="AutoBandWindowHistory.xml" />-->
		<File name="AutoBandWindow.xml"/>
		
	</Files>

	<SavedVariables>
		<SavedVariable name="AutoBand.saved"/>
	</SavedVariables>

	<OnInitialize>
		<CreateWindow name="AutoBandMapIcon" show="true"/>
		<CallFunction name="AutoBand.init"/>
		<CreateWindow name="AutoBandWindow" show="false"/>
		<CreateWindow name="AutoBandWindowTemplateSave" show="false"/>
	</OnInitialize>

	<OnUpdate>
		<CallFunction name="AutoBand.update"/>
	</OnUpdate>

	<OnShutdown/>
</UiMod>
</ModuleFile>