-- ab_util.lua
-- Utility functions

AB_util = {}



-- return "rdps", "mdps", "healer", "tank" or nil (error)
function AB_util.career_category(cid)
	return AB_const.CAREERLINE_MAP[cid]
end


function AB_util.shout(text)
	TextLogAddEntry ("Chat", SystemData.ChatLogFilters.SHOUT, towstring(text))
end


function AB_util.print(text)
	EA_ChatWindow.Print(L"[AutoBand] " .. towstring(text))
end


function AB_util.debug(stuff)
	if (AutoBand.debugon) then
		d(stuff)
	end
end


-- iterates over every player in data calling "f(group id, player object, player position)"
function AB_util.foreach_player(data, f)
	for i, group in pairs(data) do
		for j, player in pairs(group) do
			f(i, player, j)
		end
	end
end


-- returns a COPY of a scenario party in player[i][j] format
function AB_util.get_hot_scdata()
	local newwb = {}
	local scdata = GameData.GetScenarioPlayerGroups()	-- get scenario wb
	for _, player in ipairs(scdata) do
		if (newwb[player.sgroupindex] == nil) then
			newwb[player.sgroupindex] = {}		-- create group
		end
		newwb[player.sgroupindex][player.sgroupslotnum] = player
	end
	return newwb
end


-- returns a COPY of a warband party in player[i][j] format
function AB_util.get_hot_wbdata()
	local newwb = {}
	local wbdata = GetBattlegroupMemberData()		-- get warband
	for i in ipairs(wbdata) do
		newwb[i] = {}
	end
	for gid, group in ipairs(wbdata) do
		for pid, player in ipairs(group.players) do
			table.insert(newwb[gid], player)
		end
	end
	return newwb
end


-- return wb size
function AB_util.size_wb(wb)
	local count = 0
	if (wb) then
		for _, grp in pairs(wb) do
			count = count + (#grp)
		end
	end
	return count
end

function AB_util.size_table(t)
	local count = 0
	if (t) then
		for _, grp in pairs(t) do
			count = count + 1
		end
	end
	return count
end


function AB_util.print_wb(wb)
	AB_util.foreach_player(wb,
		function (gid, player, pid)
			AB_util.debug("gid " .. gid .. " " .. player)
		end
	)
end


--[[
function AB_util.bufftype(buff)
	if (buff.isHex or buff.isCurse or buff.isCripple or buff.isAilment or buff.isDamaging or buff.isDebuff) then
		return "DEBUFF"
	else
		return "BUFF"
	end
end


function AB_util.complexbufftype(buff)
	if (buff.isHex) then
		return "HEX"
	elseif (buff.isCurse) then
		return "CURSE"
	elseif (buff.isCripple) then
		return "CRIPPLE"
	elseif (buff.isAilment) then
		return "AILMENT"
	elseif (buff.isDamaging) then
		return "DAMAGING"
	elseif (buff.isDebuff) then
		return "DEBUFF"
	else
		return "BUFF"
	end
end
]]--

--[[ Notes: please dont delete
t = { 3,2,5,1,4 }
table.sort(t, function(a,b) return a<b end)	-- sort incrementing	1, 2, 3, 4, 5 
table.sort(t, function(a,b) return a>b end)	-- sort decrementing	5, 4, 3, 2, 1
local toc = TomeGetNoteworthyPersonsTOC()
local zone = GetZoneName(i);
]]--