AutoBandWindowTemplateSave = {}
AutoBandWindowTemplateSave.name = "AutoBandWindowTemplateSave"
AutoBandWindowTemplateSave.callback = nil

function AutoBandWindowTemplateSave.Initialize()
	LabelSetText(AutoBandWindowTemplateSave.name .. "TitleBarText", L"New Template")
	LabelSetText(AutoBandWindowTemplateSave.name .. "NameLabel", L"Enter a name:")
	ButtonSetText(AutoBandWindowTemplateSave.name .. "OKButton", L"OK")
	ButtonSetText(AutoBandWindowTemplateSave.name .. "CancelButton", L"Cancel")
end

function AutoBandWindowTemplateSave.OnOK()
	local newname = tostring(TextEditBoxGetText(AutoBandWindowTemplateSave.name .. "NameEditBox"))
	AB_util.debug("OK: " .. newname)
	if (AutoBandWindowTemplateSave.callback and newname ~= "" and not AutoBandWindowTemplate.is_special_template(newname)) then
		AutoBandWindowTemplateSave.Hide()
		AutoBandWindowTemplateSave.callback(newname)
	end
end

function AutoBandWindowTemplateSave.OnCancel()
	AB_util.debug("Cancel")
	AutoBandWindowTemplateSave.Hide()
end

function AutoBandWindowTemplateSave.Hide()
	WindowSetShowing(AutoBandWindowTemplateSave.name, false)
	-- mask down
	WindowSetShowing(AutoBandWindowTemplate.name .. "Mask", false)
end

function AutoBandWindowTemplateSave.ShowModal(callback)
	AutoBandWindowTemplateSave.callback = callback
	WindowSetShowing(AutoBandWindowTemplateSave.name, true)
end