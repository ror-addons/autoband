--
AutoBandWindowTools = {}

AutoBandWindowTools.name = "AutoBandWindowToolsTab"

AutoBandWindowTools.tools_data = {
	[AB_const.KICK_OFFLINE] = { ["button"] = "KickOfflineCheckBox" , ["label"] = "KickOfflineLabel", ["func_kick"]= AutoBand.cmd_kick_offline, ["func_print"] = AutoBand.cmd_list_offline},
	[AB_const.KICK_ZONE] = { ["button"] = "KickZoneCheckBox" , ["label"] = "KickZoneLabel", ["func_kick"]= AutoBand.cmd_kick_notinzone, ["func_print"] = AutoBand.cmd_list_zone},
	[AB_const.KICK_RANK] = { ["button"] = "KickRankCheckBox" , ["label"] = "KickRankLabel", ["func_kick"]= AutoBand.cmd_kick_rank, ["func_print"] = AutoBand.cmd_list_rank} }


function AutoBandWindowTools.Initialize()
	LabelSetText(AutoBandWindowTools.name .. "KickLabel", L"Manage Players")

	for fid, func in pairs(AutoBandWindowTools.tools_data) do
		LabelSetText(AutoBandWindowTools.name .. func["label"], towstring(AB_const.LABEL_KICK[fid]))
		ButtonSetCheckButtonFlag(AutoBandWindowTools.name .. func["button"], true)
		ButtonSetPressedFlag(AutoBandWindowTools.name .. func["button"],  AutoBand.saved.kick_func_enabled[fid])
	end

	ButtonSetText(AutoBandWindowTools.name .. "PrintButton", L"Print")
	ButtonSetText(AutoBandWindowTools.name .. "KickButton", L"Kick")

	-- Button bar
	ButtonSetText(AutoBandWindowTools.name .. "ResetButton", L"Restore Defaults")
end


function AutoBandWindowTools.Hide()
	WindowSetShowing(AutoBandWindowTools.name, false)
end


function AutoBandWindowTools.Show()
	WindowSetShowing(AutoBandWindowTools.name, true)
end


function AutoBandWindowTools.reset_kick_enabled()
	for fid, func in pairs(AutoBandWindowTools.tools_data) do
		ButtonSetPressedFlag(AutoBandWindowTools.name .. func["button"], AB_const.KICK_FUNC_DEFAULT[fid])
		AutoBand.saved.kick_func_enabled[fid] = AB_const.KICK_FUNC_DEFAULT[fid]
	end
end


function AutoBandWindowTools.OnKickOfflineCheckBox()
	AutoBand.saved.kick_func_enabled[AB_const.KICK_OFFLINE] = ButtonGetPressedFlag(AutoBandWindowTools.name .. "KickOfflineCheckBox")
end


function AutoBandWindowTools.OnKickZoneCheckBox()
	AutoBand.saved.kick_func_enabled[AB_const.KICK_ZONE] = ButtonGetPressedFlag(AutoBandWindowTools.name .. "KickZoneCheckBox")
end


function AutoBandWindowTools.OnKickRankCheckBox()
	AutoBand.saved.kick_func_enabled[AB_const.KICK_RANK] = ButtonGetPressedFlag(AutoBandWindowTools.name .. "KickRankCheckBox")
end


function AutoBandWindowTools.OnKickButton()
	for fid, kick_func in pairs(AutoBandWindowTools.tools_data) do
		-- to make sure that you read the values from check boxes
		AutoBand.saved.kick_func_enabled[fid] = ButtonGetPressedFlag(AutoBandWindowTools.name .. kick_func["button"])
		if (AutoBand.saved.kick_func_enabled[fid]) then
			kick_func.func_kick()
		end
	end
end

function AutoBandWindowTools.OnPrintButton()
	AB_util.print("Warband Report ")
	for fid, kick_func in pairs(AutoBandWindowTools.tools_data) do
		-- to make sure that you read the values from check boxes
		AutoBand.saved.kick_func_enabled[fid] = ButtonGetPressedFlag(AutoBandWindowTools.name .. kick_func["button"])
		if (AutoBand.saved.kick_func_enabled[fid]) then
			kick_func.func_print()
		end
	end
end


function AutoBandWindowTools.OnResetTools()
	AB_util.debug(AutoBand.saved.kick_func_enabled)
	AB_util.debug(AB_const.KICK_FUNC_DEFAULT)
	AutoBandWindowTools.reset_kick_enabled()
	AutoBandWindowTools.Show()
end