--
AutoBandWindowConfig = {}

AutoBandWindowConfig.name = "AutoBandWindowConfigTab"
AutoBandWindowConfig.combobox_name = AutoBandWindowConfig.name .. "ComboBox"


function AutoBandWindowConfig.Initialize()
	-- Settings: Templates
	LabelSetText(AutoBandWindowConfig.name .. "DefaultLabel", L"Default Template")
	ButtonSetText(AutoBandWindowConfig.name .. "ClearTemplateButton", L"Reset")

	AutoBandWindowConfig.refresh_default_template() 	

	LabelSetText(AutoBandWindowConfig.name .. "ComboBoxLabel", L"Distribution Algorithm:")
	AutoBandWindowConfig.init_combobox()
	AutoBandWindowConfig.refresh_combobox()

	-- Settings: Auto Kick Offliners 
	LabelSetText(AutoBandWindowConfig.name .. "AutoKickLabel", L"Auto Kick Offliners")
	ButtonSetCheckButtonFlag(AutoBandWindowConfig.name .. "KickCheckBox", true)
	
	LabelSetText(AutoBandWindowConfig.name .. "KickTimeLabel", L"Timeout (min)") -- (min)")
	AutoBandWindowConfig.refresh_kickoff()	

	-- Settings: Rank requirement
	LabelSetText(AutoBandWindowConfig.name .. "RankReqLabel", L"Rank Requirement")
--	LabelSetText(AutoBandWindowConfig.name .. "MinRankLabel", L"Minimum")

	-- Minimap icon
	LabelSetText(AutoBandWindowConfig.name .. "MapIconLabel", L"Minimap icon")
	ButtonSetCheckButtonFlag(AutoBandWindowConfig.name .. "MapIconCheckBox", true)
--	ButtonSetPressedFlag(AutoBandWindowConfig.name .. "MapIconCheckBox", AutoBand.saved.displayicon)

	AutoBandWindowConfig.refresh_min_rank()	
	
	LabelSetText(AutoBandWindowConfig.name .. "VersionLabel", L"v" .. towstring(AB_const.VERSION))

	-- Button bar
	ButtonSetText(AutoBandWindowConfig.name .. "ResetButton", L"Restore Defaults")
	WindowSetShowing(AutoBandWindowConfig.name .. "DisplaySeperator1", false) 
	WindowSetShowing(AutoBandWindowConfig.name .. "DisplaySeperator2", true) 
	WindowSetShowing(AutoBandWindowConfig.name .. "DisplaySeperator3", false) 
end

function AutoBandWindowConfig.refresh_default_template()
	local clear_default = true
	local default_template = AB_const.EMPTY_TEMPLATE

	if (AutoBand.saved.default_template ~= AB_const.EMPTY_TEMPLATE) then
		clear_default = false
		default_template = AutoBand.saved.default_template
		AB_util.debug("Default is not set to automatic: " .. default_template)
	end

	LabelSetText(AutoBandWindowConfig.name .. "DefaultTemplateLabel", towstring(default_template))	
	ButtonSetDisabledFlag(AutoBandWindowConfig.name .. "ClearTemplateButton", clear_default)
end


function AutoBandWindowConfig.init_combobox()
	-- reloadui combobox
	AutoBandWindowConfig.combo_items = {}
	ComboBoxClearMenuItems(AutoBandWindowConfig.combobox_name)
	-- add from templates
	for mid, mode in pairs(AB_const.ALGO_MODE) do
		ComboBoxAddMenuItem(AutoBandWindowConfig.combobox_name, towstring("(" .. mid .. ") " .. mode))
		table.insert(AutoBandWindowConfig.combo_items, "(" .. mid .. ") " .. mode)
	end
end

function AutoBandWindowConfig.refresh_combobox()
	ComboBoxSetSelectedMenuItem(AutoBandWindowConfig.combobox_name, AutoBand.saved.org_algo_mode)
end

-- return selected text
function AutoBandWindowConfig.selected_combobox()
	return tostring(ComboBoxGetSelectedText(AutoBandWindowConfig.combobox_name))
end

-- load selected algorithm
function AutoBandWindowConfig.OnSelChangedComboBox()
	local name = AutoBandWindowConfig.selected_combobox()
	local index = nil
	for i, v in ipairs(AutoBandWindowConfig.combo_items) do
		if (v == name) then
			index = i
			break
		end
	end
	if (index) then
		AutoBand.saved.org_algo_mode = index
		ComboBoxSetSelectedMenuItem(AutoBandWindowConfig.combobox_name, AutoBand.saved.org_algo_mode)
		AB_util.debug(AutoBand.saved.org_algo_mode)
	end
end

function AutoBandWindowConfig.Hide()
	WindowSetShowing(AutoBandWindowConfig.name, false)
end

function AutoBandWindowConfig.Show()
	AutoBandWindowConfig.refresh_kickoff()
	AutoBandWindowConfig.refresh_default_template()
	AutoBandWindowConfig.refresh_combobox()
	AutoBandWindowConfig.refresh_min_rank()
	ButtonSetPressedFlag(AutoBandWindowConfig.name .. "MapIconCheckBox", AutoBand.saved.displayicon)
	AutoBandWindow.toggle_mapicon(AutoBand.saved.displayicon)
	WindowSetShowing(AutoBandWindowConfig.name, true)
end

function AutoBandWindowConfig.OnClearTemplateButton()
	if (ButtonGetDisabledFlag(AutoBandWindowConfig.name .. "ClearTemplateButton")) then
		return
	end
	AutoBand.saved.default_template = AB_const.EMPTY_TEMPLATE
	AutoBandWindowConfig.refresh_default_template()
end

function AutoBandWindowConfig.OnPressTimeMinusButton()
	AutoBand.saved.autokick_period = AutoBand.saved.autokick_period - 1
	if (AutoBand.saved.autokick_period < 0) then
		AutoBand.saved.autokick_period = AB_const.MAX_KICKOFF_TIME
	end
	LabelSetText(AutoBandWindowConfig.name .. "AutoKickTimeoutLabel", L"" .. AutoBand.saved.autokick_period)
end

function AutoBandWindowConfig.OnPressTimePlusButton()
	AutoBand.saved.autokick_period = AutoBand.saved.autokick_period + 1
	if (AutoBand.saved.autokick_period > AB_const.MAX_KICKOFF_TIME) then
		AutoBand.saved.autokick_period = 0
	end
	LabelSetText(AutoBandWindowConfig.name .. "AutoKickTimeoutLabel", L"" .. AutoBand.saved.autokick_period)
end


function AutoBandWindowConfig.OnPressRankMinusButton()
	AutoBand.saved.min_rank = AutoBand.saved.min_rank - 1
	if (AutoBand.saved.min_rank < 0) then
		AutoBand.saved.min_rank = AB_const.MAXIMUM_RANK 
	end
	LabelSetText(AutoBandWindowConfig.name .. "MinRankValueLabel", L"" .. AutoBand.saved.min_rank)
end

function AutoBandWindowConfig.OnPressRankPlusButton()
	AutoBand.saved.min_rank = AutoBand.saved.min_rank + 1
	if (AutoBand.saved.min_rank > AB_const.MAXIMUM_RANK ) then
		AutoBand.saved.min_rank = 0
	end
	LabelSetText(AutoBandWindowConfig.name .. "MinRankValueLabel", L"" .. AutoBand.saved.min_rank)
end

function AutoBandWindowConfig.refresh_kickoff() 
	LabelSetText(AutoBandWindowConfig.name .. "AutoKickTimeoutLabel", L"" .. AutoBand.saved.autokick_period)
	ButtonSetPressedFlag(AutoBandWindowConfig.name .. "KickCheckBox", AutoBand.saved.autokick_enabled)
end


function AutoBandWindowConfig.refresh_min_rank() 
	LabelSetText(AutoBandWindowConfig.name .. "MinRankValueLabel", L"" .. AutoBand.saved.min_rank)
end

function AutoBandWindowConfig.OnKickCheckBox()
	AutoBand.saved.autokick_enabled = ButtonGetPressedFlag(AutoBandWindowConfig.name .. "KickCheckBox")
end


function AutoBandWindowConfig.OnResetConfig()
	AutoBand.saved.autokick_enabled = AB_const.AUTOKICK
	AutoBand.saved.autokick_period = AB_const.KICK_PERIOD
	AutoBand.saved.default_template = AB_const.EMPTY_TEMPLATE
	AutoBand.saved.org_algo_mode = AB_const.DEFAULT_ORG_ALGO
	AutoBand.saved.min_rank = AB_const.MINIMUM_RANK
	AutoBand.saved.displayicon = true
	AutoBandWindowConfig.Show()
end


function AutoBandWindowConfig.OnMapIconCheckBox()
	AutoBandWindow.toggle_mapicon(ButtonGetPressedFlag(AutoBandWindowConfig.name .. "MapIconCheckBox"))
end