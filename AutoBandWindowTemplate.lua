--
AutoBandWindowTemplate = {}

AutoBandWindowTemplate.name = "AutoBandWindowTemplateTab"
AutoBandWindowTemplate.combobox_name = AutoBandWindowTemplate.name .. "ComboBox"
AutoBandWindowTemplate.applybutton_name = AutoBandWindowTemplate.name .. "ApplyButton"
AutoBandWindowTemplate.deletebutton_name = AutoBandWindowTemplate.name .. "DeleteButton"
AutoBandWindowTemplate.savebutton_name = AutoBandWindowTemplate.name .. "SaveButton"
AutoBandWindowTemplate.defaultcheckbox_name = AutoBandWindowTemplate.name .. "DefaultCheckBox"

AutoBandWindowTemplate.group_roles = {}
AutoBandWindowTemplate.group_checkbox = {}
AutoBandWindowTemplate.modified = false
AutoBandWindowTemplate.combo_items = {}


function AutoBandWindowTemplate.Initialize()
	LabelSetText(AutoBandWindowTemplate.name .. "ComboBoxLabel", L"Templates:")

	LabelSetText(AutoBandWindowTemplate.name .. "DefaultLabel", L"Default")
	ButtonSetCheckButtonFlag(AutoBandWindowTemplate.defaultcheckbox_name, true)

	ButtonSetDisabledFlag(AutoBandWindowTemplate.savebutton_name, true)
	ButtonSetText(AutoBandWindowTemplate.savebutton_name, L"Save")
	ButtonSetText(AutoBandWindowTemplate.deletebutton_name, L"Delete")
	ButtonSetText(AutoBandWindowTemplate.applybutton_name, L"Apply")

	-- add the empty template automatic
	AutoBand.saved.templates[AB_const.EMPTY_TEMPLATE] = nil

	-- mask down
	WindowSetShowing(AutoBandWindowTemplate.name .. "Mask", false)

	AutoBandWindowTemplate.create_role_labels()

	AutoBandWindowTemplate.refresh_gui()
	AutoBandWindowTemplate.set_combobox_item(AB_const.EMPTY_TEMPLATE)
end

function AutoBandWindowTemplate.Hide()
	WindowSetShowing(AutoBandWindowTemplate.name, false)
end

function AutoBandWindowTemplate.Show()
	ButtonSetPressedFlag(AutoBandWindowTemplate.defaultcheckbox_name, AutoBandWindowTemplate.selected_combobox() == AutoBand.saved.default_template)
	WindowSetShowing(AutoBandWindowTemplate.name, true)
end

-- default template checkbox pressed
function AutoBandWindowTemplate.OnDefaultCheckBox()
	if (ButtonGetDisabledFlag(AutoBandWindowTemplate.defaultcheckbox_name)) then
		return
	end

	local checked = ButtonGetPressedFlag(AutoBandWindowTemplate.defaultcheckbox_name)
	if (checked) then
		AutoBand.saved.default_template = AutoBandWindowTemplate.selected_combobox()
	else
		AutoBand.saved.default_template = AB_const.EMPTY_TEMPLATE
	end
end

function AutoBandWindowTemplate.is_special_template(name)
	return (name == AB_const.CURRENT_WB) or (name == AB_const.EMPTY_TEMPLATE)
end

-- well, loads a template
function AutoBandWindowTemplate.load_template(name)
	AB_util.debug("Loading: " .. name)

	-- check if it's the default
	ButtonSetPressedFlag(AutoBandWindowTemplate.defaultcheckbox_name, name == AutoBand.saved.default_template)

	local temp = nil
	local is_special = AutoBandWindowTemplate.is_special_template(name)
	ButtonSetDisabledFlag(AutoBandWindowTemplate.defaultcheckbox_name, is_special)
--	ButtonSetDisabledFlag(AutoBandWindowTemplate.deletebutton_name, is_special)

	ButtonSetDisabledFlag(AutoBandWindowTemplate.applybutton_name, false)
	if (is_special) then
		if (name == AB_const.CURRENT_WB) then
			temp = AB_template.from_wb(AutoBand.get_wb())
			ButtonSetDisabledFlag(AutoBandWindowTemplate.applybutton_name, true)	-- no point applying warband to current warband
		elseif (name == AB_const.EMPTY_TEMPLATE) then
			temp = AB_const.EMPTY_WB_TEMPLATE
		end
		ButtonSetText(AutoBandWindowTemplate.deletebutton_name, L"Reset")
	else
		temp = AutoBand.saved.templates[name]
		ButtonSetText(AutoBandWindowTemplate.deletebutton_name, L"Delete")
	end

	if (temp) then
	--	AB_util.debug("Loading: template")
	--	AB_util.debug(temp)
		for gid = 1, AB_const.MAX_WB_GROUPS do
			for pid = 1, AB_const.GROUP_SIZE do
				local labelname = AutoBandWindowTemplate.group_roles[gid][pid]
				if (temp[gid][pid] == nil) then
					AutoBandWindowTemplate.set_role_label(labelname, AB_const.LABEL_EMPTY)
				else
					AutoBandWindowTemplate.set_role_label(labelname, temp[gid][pid].role)
				end
			end
		end
	end

	AutoBandWindowTemplate.modified = false
	ButtonSetDisabledFlag(AutoBandWindowTemplate.savebutton_name, true)
end

-- load selected template from combobox selected
function AutoBandWindowTemplate.OnSelChangedComboBox()
	AutoBandWindowTemplate.load_template(AutoBandWindowTemplate.selected_combobox())
end

-- save template
function AutoBandWindowTemplate.OnSaveTemplate()
	if (ButtonGetDisabledFlag(AutoBandWindowTemplate.savebutton_name)) then
		return
	end

	-- mask up
	WindowSetShowing(AutoBandWindowTemplate.name .. "Mask", true)

	AutoBandWindowTemplateSave.ShowModal(
		-- this get called back if the user pressed OK and typed something
		function(name)
			-- save in AutoBand.saved.templates
			AutoBand.saved.templates[name] = AutoBandWindowTemplate.get_template_from_labels()
			-- reset checkboxes
			for gid = 1, AB_const.MAX_WB_GROUPS do
				ButtonSetPressedFlag(AutoBandWindowTemplate.group_checkbox[gid], true)
			end
			AutoBandWindowTemplate.modified = false
			-- refresh
			AutoBandWindowTemplate.refresh_gui()
			AutoBandWindowTemplate.set_combobox_item(name)
		end
	)
end

-- delete template
function AutoBandWindowTemplate.OnDeleteTemplate()
	if (ButtonGetDisabledFlag(AutoBandWindowTemplate.deletebutton_name)) then
		return
	end

	local sel = AutoBandWindowTemplate.selected_combobox()
	if (AutoBandWindowTemplate.is_special_template(sel)) then
--		AB_util.debug("Cannot delete " .. sel)
		-- we will refresh if it's <warband> or reset if it's <automatic>, in any case just reload them
		AutoBandWindowTemplate.load_template(sel)
		return
	end
	AB_util.debug("Deleting: " .. sel)
	if (AutoBand.saved.templates[sel] ~= nil) then
		AutoBand.saved.templates[sel] = nil
		if (AutoBand.saved.default_template == sel) then
			AutoBand.saved.default_template = AB_const.EMPTY_TEMPLATE
		end
		AutoBandWindowTemplate.refresh_gui()
		AutoBandWindowTemplate.set_combobox_item(AB_const.EMPTY_TEMPLATE)
	end
end

-- apply template (organize a warband)
function AutoBandWindowTemplate.OnApplyTemplate()
	if (ButtonGetDisabledFlag(AutoBandWindowTemplate.applybutton_name)) then
		return
	end

	if (AutoBand.has_permission() or AutoBand.debugon) then
		AB_org.auto_organize(AutoBandWindowTemplate.get_template_from_labels())
	end
end

-- create a template from labels
function AutoBandWindowTemplate.get_template_from_labels()
	local temp = {}
	for gid = 1, AB_const.MAX_WB_GROUPS do
		temp[gid] = {}
		-- if the group checkbox is checked
		if (ButtonGetPressedFlag(AutoBandWindowTemplate.group_checkbox[gid])) then
			for pid = 1, AB_const.GROUP_SIZE do
				local role = tostring(LabelGetText(AutoBandWindowTemplate.group_roles[gid][pid]))
				if (role ~= AB_const.LABEL_EMPTY) then
					temp[gid][pid] = {["role"] = role}
				end
			end
		end
	end
	return temp
end


-- sets role label name and color
function AutoBandWindowTemplate.set_role_label(labelname, role)
	LabelSetText(labelname , towstring(role))
	local color = AB_const.LABEL_ROLE_COLORS[role]
	LabelSetTextColor(labelname, color[1], color[2], color[3])
end

-- return selected text
function AutoBandWindowTemplate.selected_combobox()
	return tostring(ComboBoxGetSelectedText(AutoBandWindowTemplate.combobox_name))
end

-- sets combobox to item called "name"
function AutoBandWindowTemplate.set_combobox_item(name)
	local index = nil
	for i, v in ipairs(AutoBandWindowTemplate.combo_items) do
		if (v == name) then
			index = i
			break
		end
	end
	if (index) then
		ComboBoxSetSelectedMenuItem(AutoBandWindowTemplate.combobox_name, index)
		AutoBandWindowTemplate.load_template(name)
	end
end

-- reload combobox items
function AutoBandWindowTemplate.refresh_gui()
	-- enable/disable Save
	ButtonSetDisabledFlag(AutoBandWindowTemplate.savebutton_name, not AutoBandWindowTemplate.modified)
	-- reloadui combobox
	AutoBandWindowTemplate.combo_items = {}
	ComboBoxClearMenuItems(AutoBandWindowTemplate.combobox_name)
	-- add current warband
	ComboBoxAddMenuItem(AutoBandWindowTemplate.combobox_name, towstring(AB_const.EMPTY_TEMPLATE))
	table.insert(AutoBandWindowTemplate.combo_items, AB_const.EMPTY_TEMPLATE)
	ComboBoxAddMenuItem(AutoBandWindowTemplate.combobox_name, towstring(AB_const.CURRENT_WB))
	table.insert(AutoBandWindowTemplate.combo_items, AB_const.CURRENT_WB)
	-- add from templates
	for tname in pairs(AutoBand.saved.templates) do
		ComboBoxAddMenuItem(AutoBandWindowTemplate.combobox_name, towstring(tname))
		table.insert(AutoBandWindowTemplate.combo_items, tname)
	end
end

-- create GUI element and return its name
function AutoBandWindowTemplate.create_element_tl_tl(element, name, parent, xoff, yoff, width, height)
	local label_name = AutoBandWindowTemplate.name .. "Elem" .. name
	CreateWindowFromTemplate(label_name, element, parent)
	width = width or 100
	height = height or 30
	WindowSetDimensions(label_name, width, height)
	WindowAddAnchor(label_name, "topleft", parent, "topleft", xoff, yoff)
	return label_name
end

-- fired whenever a role label is left clicked
function AutoBandWindowTemplate.role_label_left_clicked()
	AutoBandWindowTemplate.rotate_role_label(-1)
end

-- fired whenever a role label is right clicked
function AutoBandWindowTemplate.role_label_right_clicked()
	AutoBandWindowTemplate.rotate_role_label(1)
end

-- rotate label by "inc"
function AutoBandWindowTemplate.rotate_role_label(inc)
	local labelname = SystemData.ActiveWindow.name
	AB_util.debug(labelname)
	local role = tostring(LabelGetText(labelname))
	local c = AB_const.LABEL_ROLE_CATEGORIES[role] + inc
	if (c <= 0) then
		c = #AB_const.LABEL_ROLE_CATEGORIES_REV
	elseif (c > #AB_const.LABEL_ROLE_CATEGORIES_REV) then
		c = 1
	end
	AutoBandWindowTemplate.set_role_label(labelname, AB_const.LABEL_ROLE_CATEGORIES_REV[c])
	AutoBandWindowTemplate.modified = true
	ButtonSetDisabledFlag(AutoBandWindowTemplate.savebutton_name, false)
	ButtonSetDisabledFlag(AutoBandWindowTemplate.applybutton_name, false)
end

-- create role labels
function AutoBandWindowTemplate.create_role_labels()
	local Y_OFF = 95
	for gid = 1, AB_const.MAX_WB_GROUPS do
		AutoBandWindowTemplate.group_roles[gid] = {}
		local xoff = ((gid - 1) * 110) + 30
		-- create label for each group
		local capname = AutoBandWindowTemplate.create_element_tl_tl("EA_Label_DefaultText", "Caption" .. gid, AutoBandWindowTemplate.name, xoff + 20, 10 + Y_OFF, 25)
		LabelSetText(capname, towstring("#" .. gid))
		-- create role labels
		for pid = 1, AB_const.GROUP_SIZE do
			local yoff = (pid * 30) + 10 + Y_OFF
			local labelname = AutoBandWindowTemplate.create_element_tl_tl("EA_Label_DefaultText", "Role" .. gid .. pid, AutoBandWindowTemplate.name, xoff, yoff, 65)
			AutoBandWindowTemplate.set_role_label(labelname, AB_const.LABEL_EMPTY)
			WindowRegisterCoreEventHandler(labelname, "OnLButtonUp", "AutoBandWindowTemplate.role_label_left_clicked")
			WindowRegisterCoreEventHandler(labelname, "OnRButtonUp", "AutoBandWindowTemplate.role_label_right_clicked")
			AutoBandWindowTemplate.group_roles[gid][pid] = labelname
		end
		-- create checkbox for each group
		local yoff = ((AB_const.GROUP_SIZE + 1) * 30) + 20 + Y_OFF
		local chkboxname = AutoBandWindowTemplate.create_element_tl_tl("EA_Button_DefaultCheckBox", "CheckBox" .. gid, AutoBandWindowTemplate.name, xoff + 20, yoff, 22, 22)
		ButtonSetCheckButtonFlag(chkboxname, true)
		ButtonSetPressedFlag(chkboxname, true)
		AutoBandWindowTemplate.group_checkbox[gid] = chkboxname
	end
end