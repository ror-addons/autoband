-- ab_wb.lua


AB_wb = {
	group = {},
	player_count = 0,
	group_count = 0,
	fullgroup_count = 0,
	remainder_count = 0,
	MAX_GROUPS = 0			-- max number of groups (wb=4, sc=6)
}


-- default ctor
function AB_wb:new(t)
	t = t or {}
	setmetatable(t, self)
	self.__index = self
	return t
end

function AB_wb:set_groups()
	self.group = {}
	self.player_count = 0 
	self.MAX_GROUPS = AB_const.MAX_WB_GROUPS
	for i= 1, AB_const.MAX_WB_GROUPS do
		self.group[i] = {}
	end
end
function AB_wb:add_to_gid(role, pid, gid)
	local player = {["name"]= "Fake_" .. gid .. "_" .. pid }
	for key, fake in pairs(AB_const.FAKE_PLAYERS[role]) do
		player[key] = fake
	end
	player.role = role
	player.online = false
	player.zid = 0
--	self.group[gid] = self.group[gid] or {}
	table.insert(self.group[gid], player)
	self.player_count = self.player_count + 1
end

-- loads group data from scenario party
function AB_wb:load_from_scdata()
	self.group = {}
	self.player_count = 0
	local scdata = GameData.GetScenarioPlayerGroups()	-- get scenario wb
	for _, player in ipairs(scdata) do
		if (self.group[player.sgroupindex] == nil) then
			self.group[player.sgroupindex] = {}	-- create group
		end
		player.role = AB_util.career_category(player.careerLine)
		self.group[player.sgroupindex][player.sgroupslotnum] = player
		self.player_count = self.player_count + 1
	end
	self.MAX_GROUPS = 6
	self:recalc_counters()
end



-- loads group data from warband party
function AB_wb:load_from_wbdata()
	self.group = {}
	self.player_count = 0
	local wbdata = GetBattlegroupMemberData()		-- get warband
	for i in ipairs(wbdata) do
		self.group[i] = {}
	end
	for gid, grp in ipairs(wbdata) do
		for pid, player in ipairs(grp.players) do
			player.role = AB_util.career_category(player.careerLine)
			table.insert(self.group[gid], player)
			self.player_count = self.player_count + 1
		end
	end
	self.MAX_GROUPS = AB_const.MAX_WB_GROUPS
	self:recalc_counters()
end


-- iterates over every player in data calling "f(group id, player object, player position)"
function AB_wb:foreach_player(func)
	for i, grp in ipairs(self.group) do
		for j, player in ipairs(grp) do
			func(i, player, j)
		end
	end
end


--
function AB_wb:recalc_counters()
	self.group_count = math.ceil(self.player_count / AB_const.GROUP_SIZE)		-- total number of groups
	self.fullgroup_count = math.floor(self.player_count / AB_const.GROUP_SIZE)	-- full groups
	self.remainder = self.player_count % AB_const.GROUP_SIZE			-- last group size
	if (self.remainder > math.floor(AB_const.GROUP_SIZE / 2)) then
		self.fullgroup_count = self.fullgroup_count + 1
		self.remainder = 0
	end
end


--
function AB_wb:print()
	AB_util.debug("groups: " .. self.group_count)
	AB_util.debug("full groups: " .. self.fullgroup_count)
	AB_util.debug("remainder group size: " .. self.remainder)
	self:foreach_player(
		function (gid, player, pid)
			AB_util.debug("gid " .. gid .. " " .. tostring(player.name) .. " " .. player.role)
		end
	)
end