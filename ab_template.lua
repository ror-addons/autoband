-- AB_template.lua

AB_template = {}


function AB_template.from_wb(wb_obj, valid_groups)
	if (valid_groups == nil) then
		valid_groups = {}
		for i = 1, wb_obj.MAX_GROUPS do
			valid_groups[i] = true
		end
	end

	local newwb = {}
	for i = 1, wb_obj.MAX_GROUPS do
		newwb[i] = {}
	end

	wb_obj:foreach_player(
		function(gid, player)
			if (valid_groups[gid] ~= nil) then
				table.insert(newwb[gid], {["role"] = player.role})
			end
		end
	)
	return newwb
end


function AB_template.match(wb_obj, wb_dist, roles_not_taken, bins, temp)
	AB_util.debug(" match init ")
	local role_per_group = {}
	for r, rid in pairs(AB_const.ROLE_CATEGORIES) do
		role_per_group[rid] = {}
		for i = 1, AB_const.MAX_WB_GROUPS do
			role_per_group[rid][i] = 0
		end
	end
	local group_ids = {}	-- ids of groups which we will distribute the players
	for i = 1, AB_const.MAX_WB_GROUPS do
		group_ids[i] = {["id"]= i, ["size"] = 0}	
	end
	local template_match_count = 0
	local template_size = AB_util.size_wb(temp)

	if (template_size > 0) then
		local temp_ids = {}
		for tgid, tgroup in ipairs(temp) do
			temp_ids[tgid] = {["id"]= tgid, ["size"] = #tgroup}
		end

		-- sort template ids by the size of template groups groups decrementing 
		table.sort(temp_ids, function (a, b) return (a.size > b.size or (a.size == b.size and a.id < b.id)) end)

		local num_match_group = wb_obj.fullgroup_count
		AB_util.debug("AB_template.match nmatch_grp " .. num_match_group)

		local loop_watchdog = 1
		for i = 1, #temp do
			local tgid = temp_ids[i].id
			local tgroup = temp[tgid]
			for _, tplayer in ipairs(tgroup) do
				if (loop_watchdog > AB_const.LOOP_MAX) then
					AB_util.debug("****[ERROR]terminated infinite")
					exit(1)
				end
				loop_watchdog = loop_watchdog + 1
				local role_id = AB_const.ROLE_CATEGORIES[tplayer.role]
				local bin = bins[role_id]
				if (#bin > 0) then
					if (group_ids[tgid].size == 0) then
						-- found a member of new match group
						num_match_group = num_match_group - 1
					end
					local prole = table.remove(bin)
					table.insert(wb_dist[tgid], prole.role)
					table.insert(roles_not_taken[prole.role], tgid)
					template_match_count = template_match_count + 1
					group_ids[tgid].size = group_ids[tgid].size + 1
					role_per_group[role_id][tgid] = role_per_group[role_id][tgid] + 1
				else	
					AB_util.debug(" no " .. tplayer.role .. " left ")
				end
			end
			if (num_match_group <= 0) then
				break
			end
		end
		-- sort ids by the size of groups decrementing 
		table.sort(group_ids, function (a, b) return (a.size > b.size or (a.size == b.size and a.id < b.id)) end)
	end

	if (AB_util.size_wb(bins) > 0) then
		if (AutoBand.saved.org_algo_mode == AB_const.MODE_SPREAD) then
			AB_template.from_bins_spread(wb_dist, roles_not_taken, bins, role_per_group, wb_obj.fullgroup_count, group_ids)
		else
			AB_template.from_bins_aggregate(wb_dist, roles_not_taken, bins, role_per_group, wb_obj.fullgroup_count, group_ids)
		end
	end

	local left_over = AB_util.size_wb(bins)
	if (AB_util.size_wb(bins) > 0) then
		-- if there are people left which could not be matched or distributed
		if (wb_obj.fullgroup_count == 4) then
			AB_util.debug("[error] wb_obj.fullgroup_count = 4, could not place everyone")
		end
		local last_gid = group_ids[wb_obj.fullgroup_count + 1].id
		AB_template.left_over(wb_dist, roles_not_taken, bins, last_gid)
	end
	
end

function AB_template.find_min_groupid(bid, ngrp, group_ids, wb_dist, role_per_group)
	local opt_role_ratio = AB_const.GROUP_SIZE
	local opt_size = AB_const.GROUP_SIZE
	local opt_gid = -1
	for _id = 1, ngrp do
		local _gid = group_ids[_id].id
		local _gsize = group_ids[_id].size
		if (#wb_dist[_gid] < AB_const.GROUP_SIZE) then
			local roleratio = role_per_group[bid][_gid]
			if (roleratio < opt_role_ratio
			or (roleratio == opt_role_ratio and _gsize < opt_size)
			or (roleratio == opt_role_ratio and _gsize == opt_size and _gid < opt_gid)) then
				opt_role_ratio = roleratio
				opt_gid = _gid
				opt_size = _gsize
			end
		end
	end
	if (opt_gid < 0) then
		AB_util.debug("****[ERROR]terminated no gid found" .. opt_gid)
		exit(1)
	end
	return opt_gid
end


function AB_template.find_max_groupid(bid, ngrp, group_ids, wb_dist, role_per_group)
	local opt_role_ratio = -1
	local opt_size = 0
	local opt_gid = -1 
	for _id = 1, ngrp do
		local _gid = group_ids[_id].id
		local _gsize = group_ids[_id].size
		if (#wb_dist[_gid] < AB_const.GROUP_SIZE) then
			local roleratio = role_per_group[bid][_gid]
			if (roleratio > opt_role_ratio
			or (roleratio == opt_role_ratio and opt_role_ratio == 0 and _gsize < opt_size) 
			or (roleratio == opt_role_ratio and opt_role_ratio > 0 and _gsize > opt_size)) then
				opt_role_ratio = roleratio
				opt_gid = _gid
				opt_size = _gsize
			end
		end
	end
	if (opt_gid < 0) then
		AB_util.debug("****[ERROR]terminated no gid found" .. opt_gid)
		exit(1)
	end
	return opt_gid
end

-- distributes the roles to expected roles from "bins" after the templated matched, group_ids need to be sorted by their size (decrementing) 
function AB_template.from_bins_spread(wb_dist, roles_not_taken, bins, role_per_group, ngrp, group_ids)
	local size = (ngrp * AB_const.GROUP_SIZE)
	local role_count_distributed = AB_util.size_wb(wb_dist)
	local bins_size = AB_util.size_wb(bins)	
	if (bins_size < (size - role_count_distributed)) then	--if i have less people left then i want to distribute
		size = bins_size
		role_count_distributed = 0
	end
	local num_roles_per_round = ngrp

	for _id = 1, ngrp do
		local _gid = group_ids[_id].id
		if (#wb_dist[_gid] == AB_const.GROUP_SIZE) then
			num_roles_per_round = num_roles_per_round - 1
		end
	end

	local loop_watchdog = 1
	local initial_gid = 1
	local bid = 1
	while (role_count_distributed < size) do
		local bin = bins[bid]
		local i = #bin
		if (num_roles_per_round < i and AutoBand.saved.org_algo_mode ~= AB_const.MODE_AGGREGATE) then
			i = num_roles_per_round
		end
		while (i > 0 and (role_count_distributed < size)) do
			-- to avoid infinite loop if all groups are full
			if (loop_watchdog > AB_const.LOOP_MAX) then
				AB_util.debug("****[ERROR]terminated infinite")
				exit(1)
			end
			loop_watchdog = loop_watchdog + 1

			local opt_gid = AB_template.find_min_groupid(bid, ngrp, group_ids, wb_dist, role_per_group)

			local prole = table.remove(bin)
			table.insert(wb_dist[opt_gid], prole.role)
			table.insert(roles_not_taken[prole.role], opt_gid)
			group_ids[opt_gid].size = group_ids[opt_gid].size + 1
			role_per_group[bid][opt_gid] = role_per_group[bid][opt_gid] + 1

			role_count_distributed = role_count_distributed + 1
			i = i - 1
			if (#wb_dist[opt_gid] == AB_const.GROUP_SIZE) then
				num_roles_per_round = num_roles_per_round - 1
			end
		end
		bid = bid + 1
		if (bid > #bins) then
			bid = 1
		end
	end
end

-- distributes the roles to expected roles from "bins" after the templated matched, group_ids need to be sorted by their size (decrementing) 
function AB_template.from_bins_aggregate(wb_dist, roles_not_taken, bins, role_per_group, ngrp, group_ids)
	local size = (ngrp * AB_const.GROUP_SIZE)
	local role_count_distributed = AB_util.size_wb(wb_dist)
	local bins_size = AB_util.size_wb(bins)	
	if (bins_size < (size - role_count_distributed)) then	--if i have less people left then i want to distribute
		size = bins_size
		role_count_distributed = 0
	end

	local loop_watchdog = 1
	local initial_gid = 1
	local bid = 1
	while (role_count_distributed < size) do
		local bin = bins[bid]
		local i = #bin
		while (i > 0 and (role_count_distributed < size)) do
			-- to avoid infinite loop if all groups are full
			if (loop_watchdog > AB_const.LOOP_MAX) then
				AB_util.debug("****[ERROR]terminated infinite")
				exit(1)
			end
			loop_watchdog = loop_watchdog + 1

			if (bid == AB_const.ROLE_CATEGORIES[AutoBand.saved.org_algo_role]) then
				-- if it is the default role (healer)
				local opt_gid = AB_template.find_min_groupid(bid, ngrp, group_ids, wb_dist, role_per_group)

				local prole = table.remove(bin)
				table.insert(wb_dist[opt_gid], prole.role)
				table.insert(roles_not_taken[prole.role], opt_gid)
				role_per_group[bid][opt_gid] = role_per_group[bid][opt_gid] + 1
				group_ids[opt_gid].size = group_ids[opt_gid].size + 1
				role_count_distributed = role_count_distributed + 1
				i = i - 1
			else
				local opt_gid = AB_template.find_max_groupid(bid, ngrp, group_ids, wb_dist, role_per_group)

				local place = i
				if ((AB_const.GROUP_SIZE - #wb_dist[opt_gid]) < place) then
					place = AB_const.GROUP_SIZE - #wb_dist[opt_gid]
				end
				for _id = 1, place do
					local prole = table.remove(bin)
					table.insert(wb_dist[opt_gid], prole.role)
					table.insert(roles_not_taken[prole.role], opt_gid)
					role_per_group[bid][opt_gid] = role_per_group[bid][opt_gid] + 1
					group_ids[opt_gid].size = group_ids[opt_gid].size + 1
					role_count_distributed = role_count_distributed + 1
					i = i - 1
				end
				i = 0 --force proceding to next role (loop ends)
			end
		end
		bid = bid + 1
		if (bid > #bins) then
			bid = 1
		end
	end
end


function AB_template.left_over(wb_dist, roles_not_taken, bins, last_gid)
	for key, bin in ipairs(bins) do
		local bin_size = #bin
		for pid = bin_size, 1, -1 do
			local player_role = bin[pid].role
			table.insert(wb_dist[last_gid], player_role)
			table.insert(roles_not_taken[player_role], last_gid)
			bins[key][pid] = nil
		end
	end
end